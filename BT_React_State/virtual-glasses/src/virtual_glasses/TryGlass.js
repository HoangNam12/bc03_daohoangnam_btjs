import React, { Component } from "react";
import Glass from "./Glass";
import { GlassData } from "./GlassesData";
import GlassInfo from "./GlassInfo";
export default class TryGlass extends Component {
  state = {
    glassData: GlassData,
    glassUrl: "",
    glassInfo: [],
  };
  handleRenderGlass = (glassID) => {
    let cloneGlass = [...this.state.glassData];
    let index = cloneGlass.findIndex((glass) => {
      return glass.id === glassID;
    });
    this.setState({
      glassUrl: cloneGlass[index].url,
      glassInfo: cloneGlass[index],
    });
  };
  render() {
    return (
      <div className="container vglasses py-3">
        <div className="row justify-content-between">
          <div className="col-6 vglasses__left">
            <div className="row">
              <div className="col-12">
                <h1 className="mb-2">Virtual Glasses</h1>
              </div>
            </div>
            <div className="row" id="vglassesList">
              {this.state.glassData.map((glass) => {
                return (
                  <Glass
                    glassData={glass}
                    handleRenderGlass={this.handleRenderGlass}
                  />
                );
              })}
            </div>
          </div>
          <div className="col-5 vglasses__right p-0">
            <div className="vglasses__card">
              <div
                className="vglasses__model"
                id="avatar"
                style={{ backgroundImage: "./glassesImage/model.jpg" }}
              >
                {this.state.glassUrl !== "" && (
                  <img src={this.state.glassUrl}></img>
                )}
              </div>
              {this.state.glassUrl !== "" && (
                <div id="glassesInfo" className="vglasses__info">
                  <GlassInfo glassInfo={this.state.glassInfo} />
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
