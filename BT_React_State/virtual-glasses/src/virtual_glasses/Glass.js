import React, { Component } from "react";

export default class Glass extends Component {
  render() {
    return (
      <img
        className="col-4"
        src={this.props.glassData.imageUrl}
        onClick={() => {
          this.props.handleRenderGlass(this.props.glassData.id);
        }}
      ></img>
    );
  }
}
