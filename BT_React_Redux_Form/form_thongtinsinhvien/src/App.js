import logo from "./logo.svg";
import "./App.css";
import QuanLyThongTinSV from "./form-thongtin/QuanLyThongTinSV";

function App() {
  return (
    <div className="App">
      <QuanLyThongTinSV />
    </div>
  );
}

export default App;
