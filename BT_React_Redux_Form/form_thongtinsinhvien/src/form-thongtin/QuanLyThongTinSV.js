import React, { Component } from "react";
import FormThongTin from "./FormThongTin";
import TableUserList from "./TableUserList";

export default class QuanLyThongTinSV extends Component {
  render() {
    return (
      <div className="container">
        <FormThongTin />
        <TableUserList />
      </div>
    );
  }
}
