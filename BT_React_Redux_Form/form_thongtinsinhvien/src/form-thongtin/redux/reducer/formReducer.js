import {
  ADD_USER,
  DELETE_USER,
  EDIT_USER,
  GET_INFO,
  SHOW_MESSAGE,
} from "../constant/constant";
const EMAIL_PATTERN =
  /^([a-zA-Z0-9]+)([_.-])?([a-zA-Z0-9]+)@([a-zA-Z]+)([.])([a-zA-Z.]+)$/;
const DEFAULT_USER = {
  id: "",
  name: "",
  phone: "",
  email: "",
  isEdit: false,
};
let initialState = {
  userList: [],
  userInfo: {
    id: "",
    name: "",
    phone: "",
    email: "",
    isEdit: false,
  },
  message: {
    id: "",
    name: "",
    phone: "",
    email: "",
  },
};

let validation = (user, userList, messageList) => {
  let result = { valid: false, message: [] };
  let index = userList.findIndex((item) => {
    return item.id === user.id;
  });

  if (user.id === "") {
    messageList.id = "Please enter your ID";
  } else if (isNaN(user.id)) {
    messageList.id = "ID must be number";
  } else if (index !== -1) {
    user.isEdit ? (messageList.id = "") : (messageList.id = "ID is duplicated");
  } else {
    messageList.id = "";
  }

  if (user.name === "") {
    messageList.name = "Please enter your name";
  } else {
    messageList.name = "";
  }

  if (user.phone === "") {
    messageList.phone = "Please enter your phone";
  } else if (isNaN(user.phone)) {
    messageList.phone = "Phone must be number";
  } else {
    messageList.phone = "";
  }

  if (user.email === "") {
    messageList.email = "Please enter your email";
  } else if (!user.email.match(EMAIL_PATTERN)) {
    messageList.email = "Invalid email";
  } else {
    messageList.email = "";
  }
  if (
    messageList.id === "" &&
    messageList.name === "" &&
    messageList.phone === "" &&
    messageList.email === ""
  ) {
    result.valid = true;
  }
  result.message = messageList;
  return result;
};
export const formReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case GET_INFO: {
      let name = payload.target.name;
      let value = payload.target.value;
      let cloneUserInfo = { ...state.userInfo, [name]: value };
      state.userInfo = cloneUserInfo;
      return { ...state };
    }
    case ADD_USER: {
      let cloneUserList = [...state.userList];
      let cloneMessage = { ...state.message };
      let cloneUserInfo = { ...state.userInfo };
      let result = validation(cloneUserInfo, cloneUserList, cloneMessage);

      if (result.valid) {
        if (cloneUserInfo.isEdit) {
          let index = cloneUserList.findIndex((user) => {
            return user.id === cloneUserInfo.id;
          });
          cloneUserInfo.isEdit = false;
          cloneUserList[index] = cloneUserInfo;
          cloneUserInfo = DEFAULT_USER;
        } else {
          cloneUserList.push(cloneUserInfo);
          cloneUserInfo = DEFAULT_USER;
        }
      }
      state.userInfo = cloneUserInfo;
      state.message = result.message;
      state.userList = cloneUserList;
      return { ...state };
    }
    case EDIT_USER: {
      let cloneUserList = [...state.userList];
      let index = cloneUserList.findIndex((user) => {
        return user.id === payload.id;
      });
      cloneUserList[index].isEdit = true;
      state.userInfo = cloneUserList[index];
      return { ...state };
    }
    case DELETE_USER: {
      let cloneUserList = [...state.userList];
      cloneUserList.splice(payload - 1, 1);
      state.userList = cloneUserList;
      return { ...state };
    }
    case SHOW_MESSAGE: {
      let cloneMessage = { ...state.message };
      state.message = cloneMessage;
      return { ...state };
    }
    default:
      return state;
  }
};
