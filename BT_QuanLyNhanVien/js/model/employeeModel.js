const EXCELLENT_LEVEL = 192;
const VERY_GOOD_LEVEL = 176;
const GOOD_LEVEL = 160;
const Staff = function (
  _account,
  _name,
  _email,
  _password,
  _workDay,
  _baseSalary,
  _position,
  _workHour
) {
  this.account = _account;
  this.name = _name;
  this.email = _email;
  this.password = _password;
  this.workDay = _workDay;
  this.baseSalary = _baseSalary;
  this.position = _position;
  this.workHour = _workHour;
  this.totalSalary = function () {
    if (this.position == 1) {
      return this.baseSalary * 3;
    } else if (this.position == 2) {
      return this.baseSalary * 2;
    } else {
      return this.baseSalary;
    }
  };
  this.rank = function () {
    if (this.workHour >= EXCELLENT_LEVEL) {
      return "Excellent";
    } else if (this.workHour >= VERY_GOOD_LEVEL) {
      return "Very Good";
    } else if (this.workHour >= GOOD_LEVEL) {
      return "Good";
    } else {
      return "Average";
    }
  };
};
