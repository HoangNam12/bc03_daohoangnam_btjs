/*-----Save data to storage----*/
const saveLocalStorage = function (list) {
  var staffListJson = JSON.stringify(list);
  localStorage.setItem(STAFFLIST_LOCAL_STORAGE, staffListJson);
};
/*-----Get data from storage----*/
const getLocalStorage = function () {
  var list = localStorage.getItem(STAFFLIST_LOCAL_STORAGE);
  if (list) {
    staffList = JSON.parse(list);
    staffList = staffList.map(function (staff) {
      return new Staff(
        staff.account,
        staff.name,
        staff.email,
        staff.password,
        staff.workDay,
        staff.baseSalary,
        staff.position,
        staff.workHour
      );
    });
  }
};
