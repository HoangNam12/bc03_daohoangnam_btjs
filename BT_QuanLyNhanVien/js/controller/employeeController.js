const BOSS = 1;
const MANAGER = 2;
const EMPLOYEE = 3;
const SEARCH_ACCOUNT = 0;
const SEARCH_NAME = 1;
const SEARCH_RANK = 0;
const SEARCH_POSITION = 1;
// Lay thong tin Nhan vien tu form
const getStaffInfor = function () {
  var staffAccount = document.getElementById("tknv").value;
  var staffName = document.getElementById("name").value;
  var staffEmail = document.getElementById("email").value;
  var staffPassword = document.getElementById("password").value;
  var staffWorkDay = document.getElementById("datepicker").value;
  var staffBaseSalary = document.getElementById("luongCB").value * 1;
  var staffPosition = document.getElementById("chucvu").value;
  var staffWorkHour = document.getElementById("gioLam").value * 1;
  return new Staff(
    staffAccount,
    staffName,
    staffEmail,
    staffPassword,
    staffWorkDay,
    staffBaseSalary,
    staffPosition,
    staffWorkHour
  );
};
// Them Nhan vien
const addStaff = function () {
  var newStaff = getStaffInfor();
  var isValidStaff = validationStaff(newStaff, staffList);
  if (isValidStaff) {
    staffList.push(newStaff);
    showStaff(staffList);
    saveLocalStorage(staffList);
    $("#myModal").modal("hide");
    document.getElementById("body-form").reset();
  }
};
// Hien thi danh sach Nhan vien
const showStaff = function (list) {
  var contentHTML = "";
  var content = "";
  var position = "";
  list.forEach((staff) => {
    if (staff.position == BOSS) {
      position = "Sếp";
    } else if (staff.position == MANAGER) {
      position = "Trưởng phòng";
    } else if (staff.position == EMPLOYEE) {
      position = "Nhân viên";
    }
    content = `
     <tr> 
        <td>${staff.account}</td>
        <td>${staff.name}</td>
        <td>${staff.email}</td>
        <td>${staff.workDay}</td>
        <td>${position}</td>
        <td>${staff.totalSalary()}</td>
        <td>${staff.rank()}</td>
        <td>
            <button class="btn btn-success" id="edit-btn" onclick="editStaff('${staff.account
      }')">Edit</button>
            <button class="btn btn-danger" id="delete-btn" onclick="deleteStaff('${staff.account
      }')">Delete</button>
        </td>
    </tr>
     `;
    contentHTML += content;
  });
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
};

// Lay vi tri Nhan vien trong danh sach
const getStaffIndex = function (mode, info, list) {
  switch (mode) {
    case SEARCH_ACCOUNT:
      var index = list.findIndex((item) => {
        return item.account == info;
      });
      return index;
    case SEARCH_NAME:
      var index = list.findIndex((item) => {
        return item.name == info;
      });
      return index;
    default:
      console.log("choose mode: 0 - account, 1 - name");
      break;
  }
};
// Hien thi thong tin Nhan vien len form
const showStaffOnModal = function (staff) {
  document.getElementById("tknv").value = staff.account;
  document.getElementById("name").value = staff.name;
  document.getElementById("email").value = staff.email;
  document.getElementById("password").value = staff.password;
  document.getElementById("datepicker").value = staff.workDay;
  document.getElementById("luongCB").value = staff.baseSalary;
  document.getElementById("chucvu").selectedIndex = staff.position;
  document.getElementById("gioLam").value = staff.workHour;
};
// Tim kiem Nhan vien
const searchStaff = function (mode, searchInfo, list) {
  var newStaffList = [];
  switch (mode) {
    case SEARCH_RANK:
      newStaffList = list.filter(function (item) {
        return item.rank().toUpperCase() == searchInfo.toUpperCase();
      });
      return newStaffList;
    case SEARCH_POSITION:
      newStaffList = list.filter(function (item) {
        return item.postion.toUpperCase() == searchInfo.toUpperCase();
      });
      return newStaffList;
    default:
      console.log("choose mode: 0 - rank, 1 - position");
      break;
  }
};
