// Bài tập 1: Tính tiền lương nhân viên
/**
 * input = số ngày làm việc, tiền lương mỗi ngày làm việc
 * step 1: Tạo biến SALARY_PER_DAY, workDayCount
 * step 2: Gán giá trị SALARY_PER_DAY, workDayCount
 * step 3: Tạo hàm tính tổng tiền
 * step 4: Tính tổng tiền và in giá trị ra màn hình
 * output = Tổng tiền lương nhận được
 */
const SALARY_PER_DAY = 100000;
var workDayCount = document.getElementById("workDay");
function SalaryCount() {
  document.getElementById("salary").value = workDayCount.value * SALARY_PER_DAY;
}
// Bài tập 2: Giá trị trung bình của số thực
/**
 * input = 5 số thực
 * step 1: Tạo biến sum, realNumber
 * step 2: Dùng vòng lặp for để lấy giá trị và tính tổng của 5 số
 * step 3: Chia tổng các số cho 5 và in giá trị ra màn hình
 * output = trung bình của 5 số thực
 */
var sum = 0;
function Average_5Number() {
  for (var i = 1; i <= 5; i++) {
    var realNumber = document.getElementById("realNumber" + i);
    console.log(realNumber.value);
    sum += realNumber.value * 1;
  }
  document.getElementById("average_5number").value = sum / 5;
}
// Bài tập 3: Quy đổi tiền
/**
 * input = Số tiền cần đổi
 * step 1: Tạo biến usd
 * step 2: Gán giá trị cho biến usd
 * step 3: Quy đổi sang tiền vnd và in ra màn hình
 * output = số tiền vnd sau quy đổi
 */
var usd = document.getElementById("USD");
function MoneyExchange() {
  document.getElementById("VND").value = usd.value * 23500;
}
// Bài tập 4: Tính chu vi và diện tích
/**
 * input = Chiều dài cạnh HCN
 * step 1: Tạo biến chieuDai, chieuRong
 * step 2: Gán giá trị cho biến chieuDai, chieuRong
 * step 3: Tính diện tích và chu vi theo công thức và in ra màn hình
 * output = Diện tích và chu vi
 */
var chieuDai = document.getElementById("chieuDai");
var chieuRong = document.getElementById("chieuRong");
function CalRec() {
  document.getElementById("chuVi").value =
    (chieuDai.value * 1 + chieuRong.value * 1) * 2;
  document.getElementById("dienTich").value = chieuDai.value * chieuRong.value;
}
// Bài tập 5: Tính tổng 2 ký số
/**
 * input = Số cần tính
 * step 1: Tạo biến num chưa số cần tính
 * step 2: Gán giá trị cho biến num
 * step 3: Dùng hàm Math.floor để lấy số hàng chục và %10 để lấy số đơn vị. Tính tổng 2 số
 * output = Tổng của 2 ký số
 */

var num = document.getElementById("num2char");
function Sum2numchar() {
  document.getElementById("sumNumChar").value =
    Math.floor(num.value / 10) + 1 * (num.value % 10);
}
