const colorList = ["pallet", "viridian", "pewter", "cerulean", "vermillion", "lavender", "celadon", "saffron", "fuschia", "cinnabar"];
let renderColorButton = () => {
    let contentHTML = "";
    let content = "";
    colorList.forEach((color) => {
        content =
            `
        <button class="color-button ${color}"></button>
        `;
        contentHTML += content;
    })
    document.getElementById("colorContainer").innerHTML = contentHTML;
    document.querySelector("#colorContainer button").className += " active";
}
let changeHouseColor = () => {
    let buttons = document.querySelectorAll("#colorContainer button");
    buttons.forEach((button) => {
        button.addEventListener("click", () => {
            let activeButton = document.getElementsByClassName("active");
            let houseColor = "house " + button.className.substring(13);
            if (activeButton.length > 0) {
                activeButton[0].className = activeButton[0].className.replace(" active", "");
            }
            button.className += " active";
            document.getElementById("house").className = houseColor;
        });
    });
}
renderColorButton();
changeHouseColor();