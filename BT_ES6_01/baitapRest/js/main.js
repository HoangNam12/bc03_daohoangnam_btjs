let calculateAverage = (...subjects) => {
  let sum = 0;
  subjects.forEach((subject) => {
    sum += subject * 1;
  });
  return sum / subjects.length;
};
document.getElementById("btnKhoi1").addEventListener("click", () => {
  let mathPoint = document.getElementById("inpToan").value;
  let physicPoint = document.getElementById("inpLy").value;
  let chemistryPoint = document.getElementById("inpHoa").value;
  let result = calculateAverage(mathPoint, physicPoint, chemistryPoint);
  document.getElementById("tbKhoi1").innerText = result.toFixed(2);
});
document.getElementById("btnKhoi2").addEventListener("click", () => {
  let literaturePoint = document.getElementById("inpVan").value;
  let historyPoint = document.getElementById("inpSu").value;
  let geographyPoint = document.getElementById("inpDia").value;
  let englishPoint = document.getElementById("inpEnglish").value;
  let result = calculateAverage(
    literaturePoint,
    historyPoint,
    geographyPoint,
    englishPoint
  );
  document.getElementById("tbKhoi2").innerText = result.toFixed(2);
});
