import React, { Component } from "react";
import { connect } from "react-redux";

class CarDetail extends Component {
  render() {
    return (
      <div>
        <table className="table table-bordered">
          <thead className="table-dark">
            <tr>
              <td colSpan="2">CAR DETAIL</td>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td className="w-50">Color</td>
              <td>{this.props.carInfo.color}</td>
            </tr>
            <tr>
              <td className="w-50"> Price</td>
              <td>{this.props.carInfo.price}</td>
            </tr>
            <tr>
              <td className="w-50">Engine Type</td>
              <td>{this.props.carInfo.engineType}</td>
            </tr>
            <tr>
              <td className="w-50">Displacement</td>
              <td>{this.props.carInfo.displacement}</td>
            </tr>
            <tr>
              <td className="w-50">Horsepower (SAE net)</td>
              <td>{this.props.carInfo.horsepower}</td>
            </tr>
            <tr>
              <td className="w-50">Torque (SAE net)</td>
              <td>{this.props.carInfo.torque}</td>
            </tr>
            <tr>
              <td className="w-50">Redline</td>
              <td>{this.props.carInfo.redline}</td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    carInfo: state.chooseCarReducer.carInfo,
  };
};
export default connect(mapStateToProps)(CarDetail);
