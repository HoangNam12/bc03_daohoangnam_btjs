import React, { Component } from "react";
import { connect } from "react-redux";
import { CHANGE_COLOR } from "./redux/constant/constants";

class IconColor extends Component {
  render() {
    return (
      <tbody>
        <tr className="d-flex flex-column">
          {this.props.carDetail.map((car) => {
            return (
              <td>
                <img
                  src={car.img}
                  style={{ width: "100px" }}
                  onClick={() => {
                    this.props.changeColor(car);
                  }}
                ></img>
              </td>
            );
          })}
        </tr>
      </tbody>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    carDetail: state.chooseCarReducer.carDetail,
  };
};
let mapDispatchToProps = (dispatch) => {
  return {
    changeColor: (item) => {
      dispatch({
        type: CHANGE_COLOR,
        payload: item,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(IconColor);
