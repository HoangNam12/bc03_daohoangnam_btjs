import logo from "./logo.svg";
import "./App.css";
import ChooseCar from "./choose-car/ChooseCar";

function App() {
  return (
    <div className="App">
      <ChooseCar />
    </div>
  );
}

export default App;
