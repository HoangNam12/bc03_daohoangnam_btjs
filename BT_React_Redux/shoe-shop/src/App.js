import logo from "./logo.svg";
import "./App.css";
import ShoesStore from "./shoe_shop/ShoesStore";

function App() {
  return (
    <div className="App">
      <ShoesStore />
    </div>
  );
}

export default App;
