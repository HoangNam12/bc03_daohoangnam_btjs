import React, { Component } from "react";
import { connect } from "react-redux";
import { ADD_TO_CART, SHOW_PRODUCT_DETAIL } from "./redux/constant/constant";

class ProductItem extends Component {
  productData = this.props.product;
  render() {
    return (
      <div className="card col-4 border-0 mb-4">
        <div className="border">
          <img
            src={this.productData.image}
            className="card-img-top"
            alt="..."
          />
          <div className="card-body">
            <h5 className="card-title">{this.productData.name}</h5>
            <p className="card-text">${this.productData.price}</p>
            <button
              className="btn btn-dark mr-3"
              onClick={() => {
                this.props.handleAddToCart(this.productData);
              }}
            >
              Add to cart
            </button>
            <button
              className="btn btn-secondary"
              data-toggle="modal"
              data-target="#productDetailModal"
              onClick={() => {
                this.props.handleProductDetail(this.productData.id);
              }}
            >
              Product Detail
            </button>
          </div>
        </div>
      </div>
    );
  }
}
let mapDispatchToProps = (dispatch) => {
  return {
    handleAddToCart: (item) => {
      dispatch({
        type: ADD_TO_CART,
        payload: item,
      });
    },
    handleProductDetail: (item) => {
      dispatch({
        type: SHOW_PRODUCT_DETAIL,
        payload: item,
      });
    },
  };
};

export default connect(null, mapDispatchToProps)(ProductItem);
