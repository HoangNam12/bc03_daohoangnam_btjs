import React, { Component } from "react";
import { connect } from "react-redux";

class ProductDetail extends Component {
  render() {
    let product = this.props.productDetail;
    return (
      <div
        className="modal fade"
        id="productDetailModal"
        tabindex="-1"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div
          className="modal-dialog modal-dialog-centered"
          style={{ maxWidth: "1000px" }}
        >
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">
                Product Detail
              </h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <div className="row mt-5">
                <div className="col-md-4">
                  <br />
                  <h3 className="text-center">{product.name}</h3>
                  <img
                    className="card-img-top"
                    src={product.image}
                    width={170}
                    height={300}
                    alt={product.name}
                  ></img>
                </div>
                <div className="col-md-8">
                  <table className="table">
                    <thead>
                      <tr>
                        <td colSpan={2} className="border-top-0">
                          <h3>Product Information</h3>
                        </td>
                      </tr>
                      <tr>
                        <td>Product Name</td>
                        <td>{product.name}</td>
                      </tr>
                      <tr>
                        <td>Price</td>
                        <td>${product.price}</td>
                      </tr>
                      <tr>
                        <td>Description</td>
                        <td>{product.description}</td>
                      </tr>
                      <tr>
                        <td>Quantity</td>
                        <td>{product.quantity}</td>
                      </tr>
                    </thead>
                  </table>
                </div>
              </div>
              <div class="modal-footer">
                <button
                  type="button"
                  class="btn btn-secondary"
                  data-dismiss="modal"
                >
                  Đóng
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    productDetail: state.shoeShopReducer.productDetail,
  };
};
export default connect(mapStateToProps)(ProductDetail);
