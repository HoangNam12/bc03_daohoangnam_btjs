import { ProductData } from "../../ProductData";
import {
  ADD_TO_CART,
  DELETE_PRODUCT,
  HANDLE_PRODUCT_NUMBER,
  SHOW_PRODUCT_DETAIL,
} from "../constant/constant";

let initialState = {
  productList: ProductData,
  cart: [],
  productDetail: [],
};
export const shoeShopReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ADD_TO_CART: {
      let productList = [...state.cart];
      let index = productList.findIndex((item) => {
        return item.id === payload.id;
      });
      if (index === -1) {
        let newProduct = { ...payload, numberInCart: 1 };
        productList.push(newProduct);
      } else {
        productList[index].numberInCart++;
      }
      state.cart = productList;
      return { ...state };
    }
    case SHOW_PRODUCT_DETAIL: {
      let index = state.productList.findIndex((product) => {
        return product.id === payload;
      });
      if (index !== -1) {
        state.productDetail = state.productList[index];
      }
      return { ...state };
    }
    case DELETE_PRODUCT: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((sanpham) => {
        return sanpham.id === payload;
      });
      if (index !== -1) {
        cloneCart.splice(index, 1);
      }
      state.cart = cloneCart;
      return { ...state };
    }
    case HANDLE_PRODUCT_NUMBER: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((product) => {
        return product.id === payload.id;
      });
      if (index !== -1) {
        cloneCart[index].numberInCart += payload.number;
      }
      cloneCart[index].numberInCart === 0 && cloneCart.splice(index, 1);
      state.cart = cloneCart;
      return { ...state };
    }
    default:
      return state;
  }
};
