const Teacher = function (
  _id,
  _username,
  _name,
  _password,
  _email,
  _type,
  _language,
  _description,
  _imageUrl
) {
  this.id = _id;
  this.username = _username;
  this.name = _name;
  this.password = _password;
  this.email = _email;
  this.type = _type;
  this.language = _language;
  this.description = _description;
  this.imageUrl = _imageUrl;
};

export { Teacher };
