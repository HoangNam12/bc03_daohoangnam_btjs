import {
  renderUserList,
  deleteUser,
  editUser,
  getUserOnForm,
  resetForm,
  updateUser,
  addUser,
} from "./userController.js";
import { getLocalStorage } from "./storageController.js";
import {
  validationUser,
  validationUpdateUser,
} from "./validationController.js";
const BASE_URL = "https://6271e185c455a64564b8efe3.mockapi.io/GV-HV";

window.deleteUser = deleteUser;
window.editUser = editUser;
renderUserList();

//----Call event when Update button is clicked--///
document.getElementById("btn-update").addEventListener("click", async () => {
  let userdata = getUserOnForm();
  let isValid = validationUpdateUser(userdata); //Validation input data on form: pass = 1; failure = 0;
  if (isValid) {
    await updateUser(userdata, BASE_URL); // call API to update user data
    renderUserList();
    resetForm();
    $("#myModal").modal("hide");
  }
});
//----Call event when Them button is clicked--///
document.getElementById("btnThemNguoiDung").addEventListener("click", () => {
  resetForm();
});
//----Call event when Add button is clicked--///
document.getElementById("btn-add").addEventListener("click", async () => {
  let newUser = getUserOnForm();
  let userList = getLocalStorage(); //Get data from local storage, faster than using API
  let isValid = validationUser(newUser, userList); //Validation input data on form: pass = 1; failure = 0;
  if (isValid) {
    await addUser(newUser, BASE_URL); //call API to add new user
    renderUserList();
    resetForm();
    $("#myModal").modal("hide");
  }
});
