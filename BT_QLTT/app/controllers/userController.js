import { Teacher } from "../models/userModel.js";
import {
  getUserListAPI,
  deleteUserAPI,
  getUserAPI,
} from "../services/axiosService.js";
import { saveLocalStorage } from "../controllers/storageController.js";
import { clearSpanError } from "../../app/controllers/validationController.js";
//----Language list to dump random language---//
const LANGUAGE_LIST = [
  "ITALIAN",
  "FRENCH",
  "JAPANESE",
  "CHINESE",
  "RUSSIAN",
  "SWEDEN",
  "SPANISH",
];
const GIAO_VIEN = 1;
const HOC_VIEN = 0;
//-----Reset all error span and data on input form---///
const resetForm = () => {
  document.getElementById("userID").style.display = "none";
  document.getElementById("TaiKhoan").disabled = false;
  document.getElementById("MoTa").textContent = "";
  document.getElementById("body-form").reset();
  clearSpanError("tbTaiKhoan");
  clearSpanError("tbHoTen");
  clearSpanError("tbEmail");
  clearSpanError("tbMatKhau");
  clearSpanError("tbHinhAnh");
  clearSpanError("tbLoaiNguoiDung");
  clearSpanError("tbLoaiNgonNgu");
  clearSpanError("tbMoTa");
};
//---Call API to get user data and show on input form--//
const showUserOnForm = async (userId) => {
  let user = await getUserAPI(userId);
  document.getElementById("userID").value = user.id;
  document.getElementById("TaiKhoan").value = user.username;
  document.getElementById("HoTen").value = user.name;
  document.getElementById("MatKhau").value = user.password;
  document.getElementById("Email").value = user.email;
  document.getElementById("loaiNguoiDung").value =
    user.type * 1 ? GIAO_VIEN : HOC_VIEN; // user.type = 1 or 0 --> convert to 1 or 0 for option value
  let select = document.getElementById("loaiNgonNgu");
  select.selectedIndex = LANGUAGE_LIST.indexOf(user.language) + 1; // LANGUAGE_LIST index + 1 = option value
  document.getElementById("MoTa").textContent = user.description;
  document.getElementById("HinhAnh").value = user.imageUrl;
};

//--Call API to get user data and show on html---//
const renderUserList = async () => {
  let userListRaw = await getUserListAPI();
  saveLocalStorage(userListRaw); //Save to local for quick validation
  let userList = userListRaw.map((user) => {
    //----Dump random language---//
    if (!LANGUAGE_LIST.includes(user.language)) {
      user.language = LANGUAGE_LIST[Math.floor(Math.random() * 7)];
    }
    return new Teacher(
      user.id,
      user.username,
      user.name,
      user.password,
      user.email,
      user.type,
      user.language,
      user.description,
      user.imageUrl
    );
  });
  let contentHTML = "";
  //---- Note: user.type = 0 or 1 ----//
  userList.forEach((user) => {
    let contenTr = `<tr>
            <td>${userList.indexOf(user) + 1}</td>
            <td>${user.username}</td>
            <td>${user.password}</td>
            <td>${user.name}</td>
            <td>${user.email}</td>
            <td>${user.language}</td>
            <td>${user.type * 1 ? "GV" : "HV"}</td> 
            <td>
                <button class="btn btn-success" onclick="editUser(${
                  user.id
                })">Edit</button>
                <button class="btn btn-danger" onclick="deleteUser(${
                  user.id
                })">Delete</button>
            </td>
        </tr>`;
    contentHTML += contenTr;
  });
  document.getElementById("tblDanhSachNguoiDung").innerHTML = contentHTML;
};
//---Delete user--//
const deleteUser = async (userId) => {
  await deleteUserAPI(userId);
  renderUserList();
};
//---Show input form when edit button is clicked--//
const editUser = async (userId) => {
  resetForm();
  $("#myModal").modal("show");
  showUserOnForm(userId);
  document.getElementById("TaiKhoan").disabled = true; //block changing username
};
//----Get data on input form and convert to Teacher object---//
let getUserOnForm = function () {
  let username = document.getElementById("TaiKhoan").value;
  let name = document.getElementById("HoTen").value;
  let password = document.getElementById("MatKhau").value;
  let email = document.getElementById("Email").value;
  let type = document.getElementById("loaiNguoiDung").value;
  let language = document.getElementById("loaiNgonNgu").value;
  let description = document.getElementById("MoTa").value;
  let imageUrl = document.getElementById("HinhAnh").value;
  let id = document.getElementById("userID").value * 1;
  return {
    id: id,
    username: username,
    name: name,
    password: password,
    email: email,
    type: type,
    language: language,
    description: description,
    imageUrl: imageUrl,
  };
};
//-----Update user data by Axios-----///
const updateUser = async (userdata, BASE_URL) => {
  let result = await axios.put(`${BASE_URL}/${userdata.id}`, userdata);
  return result.data;
};
//------Add new user data by Axios-----//
const addUser = async (newUser, BASE_URL) => {
  let result = await axios.post(`${BASE_URL}`, newUser);
  return result.data;
};
export {
  renderUserList,
  deleteUser,
  editUser,
  getUserOnForm,
  resetForm,
  updateUser,
  addUser,
};
