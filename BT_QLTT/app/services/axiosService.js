const BASE_URL = "https://6271e185c455a64564b8efe3.mockapi.io/GV-HV";

let getUserListAPI = async () => {
  let list = await axios.get(BASE_URL);
  return list.data;
};
let getUserAPI = async (id) => {
  let user = await axios.get(`${BASE_URL}/${id}`);
  return user.data;
};
let deleteUserAPI = async (id) => {
  let user = await axios.delete(`${BASE_URL}/${id}`);
  return user.data;
};

export { getUserListAPI, deleteUserAPI, getUserAPI };
