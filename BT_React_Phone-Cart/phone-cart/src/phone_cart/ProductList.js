import React, { Component } from "react";
import PhoneItem from "./PhoneItem";

export default class ProductList extends Component {
  render() {
    return (
      <div className="container">
        <h1 className="text-primary mb-5">Danh sách sản phẩm</h1>
        <div className="row">
          {this.props.productData.map((item) => {
            return (
              <PhoneItem
                data={item}
                key={item.key}
                showDetail={this.props.showDetail}
                addToCart={this.props.addToCart}
              />
            );
          })}
        </div>
      </div>
    );
  }
}
