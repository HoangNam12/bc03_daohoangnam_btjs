/**
 *
 * Bài 1: Tìm số nguyên dương nhỏ nhất
 *
 */

var sum = 0;
var n = 0;
while (sum < 10000) {
  n++;
  sum += n;
}
document.getElementById("ketquabai1").value = n;

/**
 *
 * Bài 2: Tính tổng S(n)
 *
 */

function ketquabai2() {
  var sum = 0;
  var a = document.getElementById("x").value * 1;
  var n = document.getElementById("n").value * 1;
  for (var i = 1; i <= n; i++) {
    sum += Math.pow(a, i);
  }
  document.getElementById("sum").value = sum;
}

/**
 *
 * Bài 3: Tính giai thừa
 *
 */

function ketquabai3() {
  var p = 1;
  var n = document.getElementById("number").value * 1;
  for (var i = 1; i <= n; i++) {
    p *= i;
  }
  document.getElementById("giaiThua").value = p;
}
/**
 *
 * Bài 4: Tạo 10 thẻ div
 *
 */
var contentHTML = "";
function ketquabai4() {
  for (var i = 1; i <= 10; i++) {
    if (i % 2 != 0) {
      contentHTML += `<div class="bg-primary">Div lẻ ${i}</div>`;
    } else {
      contentHTML += `<div class="bg-danger">Div chẳn ${i}</div>`;
    }
  }
  document.getElementById("createDiv").innerHTML = contentHTML;
}
/**
 *
 * Bài 5: Tìm số nguyên tố
 *
 */

function ketquabai5() {
  var contentHTML = "";
  var n = document.getElementById("soNguyenTo").value * 1;
  for (var i = 0; i <= n; i++) {
    if (i < 2) {
      contentHTML += "";
    } else {
      contentHTML += `${validateSoNguyenTo(i)} `;
    }
  }
  document.getElementById("ketquabai5").innerHTML = `<p> ${contentHTML} </p>`;
}
var validateSoNguyenTo = function (number) {
  for (var j = 2; j < number - 1; j++) {
    if (number % j == 0) {
      return "";
    }
  }
  return number;
};
