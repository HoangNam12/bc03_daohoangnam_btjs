import React, { Component } from "react";
import ProductList from "./ProductList";
import { ProductData } from "./ProductData";
import ShoesCart from "./ShoesCart";
import ProductDetail from "./ProductDetail";
export default class ShoesStore extends Component {
  state = {
    productData: ProductData,
    productDetail: [],
    cart: [],
  };
  handleAddToCart = (product) => {
    let productList = [...this.state.cart];
    let index = productList.findIndex((item) => {
      return item.id === product.id;
    });
    if (index === -1) {
      let newProduct = { ...product, numberInCart: 1 };
      productList.push(newProduct);
    } else {
      productList[index].numberInCart++;
    }
    this.setState({
      cart: productList,
    });
  };
  handleDeleteProduct = (productID) => {
    let cloneCart = [...this.state.cart];
    let index = this.state.cart.findIndex((sanpham) => {
      return sanpham.id === productID;
    });
    if (index !== -1) {
      cloneCart.splice(index, 1);
    }
    this.setState({ cart: cloneCart });
  };
  handleProductNumber = (productID, number) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((product) => {
      return product.id === productID;
    });
    if (index !== -1) {
      cloneCart[index].numberInCart += number;
    }
    cloneCart[index].numberInCart === 0 && cloneCart.splice(index, 1);
    this.setState({
      cart: cloneCart,
    });
  };
  handleProductDetail = (productID) => {
    let index = this.state.productData.findIndex((product) => {
      return product.id === productID;
    });
    if (index !== -1) {
      this.setState({
        productDetail: ProductData[index],
      });
    }
  };
  render() {
    let tongSoLuong = this.state.cart.reduce((tong, product) => {
      return (tong += product.numberInCart);
    }, 0);
    return (
      <div className="container">
        <h1>SHOE SHOP</h1>
        <div className="text-right">
          <span
            className="text-danger"
            style={{ cursor: "pointer", fontSize: "17px", fontWeight: "bold" }}
            data-toggle="modal"
            data-target="#cartModal"
          >
            <i class="fa fa-shopping-cart"></i>({tongSoLuong})
          </span>
        </div>
        <ProductList
          productData={this.state.productData}
          handleAddToCart={this.handleAddToCart}
          handleProductDetail={this.handleProductDetail}
        />
        <ShoesCart
          handleDeleteProduct={this.handleDeleteProduct}
          handleProductNumber={this.handleProductNumber}
          cartList={this.state.cart}
        />
        <ProductDetail productDetail={this.state.productDetail} />
      </div>
    );
  }
}
