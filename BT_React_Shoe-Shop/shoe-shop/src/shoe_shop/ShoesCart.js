import React, { Component } from "react";

export default class ShoesCart extends Component {
  render() {
    let total = 0;
    this.props.cartList.forEach((product) => {
      total += product.price * product.numberInCart * 1;
    });
    return (
      <div
        className="modal fade"
        id="cartModal"
        tabindex="-1"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div
          className="modal-dialog modal-dialog-centered"
          style={{ maxWidth: "1000px" }}
        >
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">
                Cart
              </h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <table className="table">
                <thead>
                  <tr>
                    <td>Product ID</td>
                    <td>Image</td>
                    <td>Product Name</td>
                    <td>Quantity</td>
                    <td>Price</td>
                    <td></td>
                  </tr>
                </thead>
                <tbody>
                  {this.props.cartList.map((sanpham) => {
                    return (
                      <tr>
                        <td>{sanpham.id}</td>
                        <td>
                          <img src={sanpham.image} width={100} />
                        </td>
                        <td>{sanpham.name}</td>
                        <td>
                          <button
                            className="btn btn-primary mx-2"
                            onClick={() => {
                              this.props.handleProductNumber(sanpham.id, 1);
                            }}
                          >
                            +
                          </button>
                          {sanpham.numberInCart}
                          <button
                            className="btn btn-primary mx-2"
                            onClick={() => {
                              this.props.handleProductNumber(sanpham.id, -1);
                            }}
                          >
                            -
                          </button>
                        </td>
                        <td>{sanpham.price}</td>
                        <td>
                          <button
                            className="btn btn-danger"
                            onClick={() => {
                              this.props.handleDeleteProduct(sanpham.id);
                            }}
                          >
                            Xoá
                          </button>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
            <div class="modal-footer">
              <div className="d-flex justify-content-between w-100">
                <h5 className="ml-5 font-weight-bold">Total: ${total}</h5>
                <button
                  type="button"
                  class="btn btn-secondary"
                  data-dismiss="modal"
                >
                  Đóng
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
