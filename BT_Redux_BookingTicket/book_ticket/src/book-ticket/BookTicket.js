import React, { Component } from "react";
import "./css/BaiTapBookingTicket.css";
import HangGhe from "./HangGhe";
import ThongTinVe from "./ThongTinVe";

export default class BookTicket extends Component {
  render() {
    return (
      <div
        className="bookingMovie"
        style={{
          backgroundImage: "url('./img/bgmovie.jpg')",
          width: "100%",
          height: "100%",
          backgroundSize: "cover",
          backgroundColor: "rbga(0,0,0,0.1)",
          backgroundAttachment: "scroll",
        }}
      >
        <div
          style={{
            backgroundColor: "rgba(0,0,0,0.6)",
            width: "100%",
            height: "100%",
            backgroundAttachment: "scroll",
          }}
        >
          <div className="row">
            <div className="col-8">
              <h1 className="text-warning mb-5 mt-5">
                Đặt vé xem phim CYBERLEARN.VN
              </h1>
              <div>
                <h5 className="text-light">MÀN HÌNH</h5>
                <div className="screen m-auto w-100"></div>
                <HangGhe />
              </div>
            </div>
            <div className="col-4">
              <ThongTinVe />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
