import React, { Component } from "react";
import { connect } from "react-redux";
import { CHOOSE_SEAT } from "./redux/constant/constants";

class HangGhe extends Component {
  render() {
    return (
      <table className="table table-borderless mt-4">
        {this.props.seatList?.map((item) => {
          return (
            <tr className="row justify-content-center">
              <td className="firstChar" style={{ width: "30px" }}>
                <span>{item.hang}</span>
              </td>
              {item.hang === "" &&
                item.danhSachGhe.map((ghe) => {
                  return <div className="rowNumber">{ghe.soGhe}</div>;
                })}
              {item.hang !== "" &&
                item.danhSachGhe.map((ghe) => {
                  return (
                    <div
                      className={
                        ghe.status
                          ? ghe.status
                          : ghe.daDat
                          ? "gheDuocChon"
                          : "ghe"
                      }
                      onClick={() => {
                        this.props.handleChooseSeat(ghe);
                      }}
                    >
                      {ghe.soGhe}
                    </div>
                  );
                })}
            </tr>
          );
        })}
      </table>
    );
  }
}
let mapDispatchToProps = (dispatch) => {
  return {
    handleChooseSeat: (item) => {
      dispatch({
        type: CHOOSE_SEAT,
        payload: item,
      });
    },
  };
};
let mapStateToProps = (state) => {
  return {
    seatList: state.bookTicketReducer.seatData,
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(HangGhe);
