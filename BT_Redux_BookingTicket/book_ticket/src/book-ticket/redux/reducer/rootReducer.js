import { bookTicketReducer } from "./bookTicketReducer";
import { combineReducers } from "redux";
export const rootReducer = combineReducers({
  bookTicketReducer,
});
