import { SeatData } from "../../SeatsData";
import {
  CONFIRM_BOOK_TICKET,
  CHOOSE_SEAT,
  CANCLE_BOOK_TICKET,
  DELETE_TICKET,
} from "../constant/constants";

let initialState = {
  seatData: SeatData,
  bookedTicket: [],
};
let findSeatIndex = (item, list) => {
  let rowIndex = list.findIndex((row) => {
    return row.hang === item.soGhe.charAt(0);
  });
  let seatIndex = list[rowIndex].danhSachGhe.findIndex((seat) => {
    return seat.soGhe === item.soGhe;
  });
  let seatInfo = { row: rowIndex, number: seatIndex };
  return seatInfo;
};

export const bookTicketReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case CONFIRM_BOOK_TICKET: {
      let cloneSeatData = [...state.seatData];
      let cloneBookTicket = [...state.bookedTicket];
      cloneBookTicket.forEach((ticket) => {
        let seat = findSeatIndex(ticket, cloneSeatData);
        ticket.daDat = true;
        cloneSeatData[seat.row].danhSachGhe[seat.number] = { ...ticket };
      });
      cloneBookTicket = [];
      state.seatData = cloneSeatData;
      state.bookedTicket = cloneBookTicket;
      return { ...state };
    }
    case CANCLE_BOOK_TICKET: {
      let cloneSeatData = [...state.seatData];
      let cloneBookTicket = [...state.bookedTicket];
      cloneBookTicket.forEach((ticket) => {
        let seat = findSeatIndex(ticket, cloneSeatData);
        ticket.daDat = false;
        cloneSeatData[seat.row].danhSachGhe[seat.number] = {
          ...ticket,
          status: false,
        };
      });
      cloneBookTicket = [];
      state.seatData = cloneSeatData;
      state.bookedTicket = cloneBookTicket;
      return { ...state };
    }
    case DELETE_TICKET: {
      let cloneSeatData = [...state.seatData];
      let cloneBookTicket = [...state.bookedTicket];
      let ticketIndex = cloneBookTicket.findIndex((ticket) => {
        return ticket.soGhe === payload.soGhe;
      });
      let seat = findSeatIndex(payload, cloneSeatData);
      payload.daDat = false;
      cloneSeatData[seat.row].danhSachGhe[seat.number] = {
        ...payload,
        status: false,
      };
      cloneBookTicket.splice(ticketIndex, 1);
      state.seatData = cloneSeatData;
      state.bookedTicket = cloneBookTicket;
      return { ...state };
    }
    case CHOOSE_SEAT: {
      let cloneSeatData = [...state.seatData];
      let cloneBookTicket = [...state.bookedTicket];
      let seat = findSeatIndex(payload, cloneSeatData);
      if (payload.daDat === false) {
        if (payload.status) {
          cloneSeatData[seat.row].danhSachGhe[seat.number] = {
            ...payload,
            status: false,
          };
          let bookTicketIndex = cloneBookTicket.findIndex((seat) => {
            return seat === payload;
          });
          cloneBookTicket.splice(bookTicketIndex, 1);
        } else {
          cloneSeatData[seat.row].danhSachGhe[seat.number] = {
            ...payload,
            status: "gheDangChon",
          };
          cloneBookTicket.push(payload);
        }
      }
      state.seatData = cloneSeatData;
      state.bookedTicket = cloneBookTicket;
      return { ...state };
    }
    default:
      return { ...state };
  }
};
