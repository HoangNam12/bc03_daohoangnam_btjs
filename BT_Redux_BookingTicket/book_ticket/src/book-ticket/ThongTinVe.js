import React, { Component } from "react";
import { connect } from "react-redux";
import {
  CANCLE_BOOK_TICKET,
  CONFIRM_BOOK_TICKET,
  DELETE_TICKET,
} from "./redux/constant/constants";

class ThongTinVe extends Component {
  render() {
    let total = this.props.bookedTicket?.reduce((tong, ticket) => {
      return (tong += ticket.gia);
    }, 0);
    return (
      <div className="container">
        <h3 className="text-light mb-5" style={{ marginTop: "10rem" }}>
          Danh sách ghế bạn chọn
        </h3>
        <div className="d-flex justify-content-start align-items-center">
          <div className="ghe ml-0"></div>
          <h3 className="text-light pl-3">Ghế chưa chọn</h3>
        </div>
        <div className="d-flex justify-content-start align-items-center">
          <div className="gheDuocChon ml-0"></div>
          <h3 className="text-light pl-3">Ghế được chọn</h3>
        </div>
        <div className="d-flex justify-content-start align-items-center">
          <div className="gheDangChon ml-0"></div>
          <h3 className="text-light pl-3">Ghế đang chọn</h3>
        </div>
        <table className="table table-bordered">
          <thead>
            <tr className="text-light">
              <td>Số ghế</td>
              <td>Giá</td>
              <td>Huỷ</td>
            </tr>
          </thead>
          {this.props.bookedTicket.map((ticket) => {
            return (
              <tr className="text-warning">
                <td>{ticket.soGhe}</td>
                <td>{ticket.gia}</td>
                <td>
                  <i
                    type="button"
                    class="fa fa-trash"
                    onClick={() => {
                      this.props.handleDeleteTicket(ticket);
                    }}
                  ></i>
                </td>
              </tr>
            );
          })}
          <tr>
            <td className="text-light">Total: </td>
            <td className="text-warning">{total ? total : ""}</td>
            <td></td>
          </tr>
        </table>
        <buton
          className="btn btn-warning mx-2"
          onClick={() => {
            this.props.handleConfirm(this.props.bookedTicket);
          }}
        >
          {" "}
          Đặt
        </buton>
        <buton
          className="btn btn-danger"
          onClick={() => {
            this.props.handleCancel(this.props.bookedTicket);
          }}
        >
          {" "}
          Huỷ
        </buton>
      </div>
    );
  }
}
let mapDispatchToProps = (dispatch) => {
  return {
    handleConfirm: (item) => {
      dispatch({
        type: CONFIRM_BOOK_TICKET,
        payload: item,
      });
    },
    handleCancel: (item) => {
      dispatch({
        type: CANCLE_BOOK_TICKET,
        payload: item,
      });
    },
    handleDeleteTicket: (item) => {
      dispatch({
        type: DELETE_TICKET,
        payload: item,
      });
    },
  };
};
let mapStateToProps = (state) => {
  return {
    bookedTicket: state.bookTicketReducer.bookedTicket,
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(ThongTinVe);
