/**Bài 1*/
var contentHTML;
function inKetQuaBai1() {
  var num1 = document.getElementById("num1").value * 1;
  var num2 = document.getElementById("num2").value * 1;
  var num3 = document.getElementById("num3").value * 1;
  contentHTML = `<p> ${Math.min(num1, num2, num3)} < ${
    num1 + num2 + num3 - Math.min(num1, num2, num3) - Math.max(num1, num2, num3)
  } < ${Math.max(num1, num2, num3)}`;
  document.getElementById("ketQuaBai1").innerHTML = contentHTML;
}

/**Bài 2*/
var contentHTML;
function inKetQuaBai2() {
  var index = document.getElementById("greetingSelect").selectedIndex;
  var choice = document.getElementsByTagName("option")[index].value;
  switch (choice) {
    case "B":
      contentHTML = `<p> Chào Bố</p>`;
      break;
    case "M":
      contentHTML = `<p> Chào Mẹ</p>`;
      break;
    case "A":
      contentHTML = `<p> Chào Anh trai </p>`;
      break;
    case "E":
      contentHTML = `<p> Chào Em gái </p>`;
      break;
    default:
      break;
  }
  document.getElementById("ketQuaBai2").innerHTML = contentHTML;
}
/**Bài 3*/
function inKetQuaBai3() {
  var count = 0;
  for (var i = 1; i <= 3; i++) {
    var num = document.getElementById("so" + i).value * 1;
    count += num % 2 == 0 ? 1 : 0;
  }
  document.getElementById("ketQuaBai3").innerHTML = `<p>Có ${count} số chẵn, ${
    3 - count
  } số lẻ`;
}
/**Bài 4*/
var contentHTML;
function inKetQuaBai4() {
  var canh1 = document.getElementById("canh1").value * 1;
  var canh2 = document.getElementById("canh2").value * 1;
  var canh3 = document.getElementById("canh3").value * 1;
  if (canh1 == canh2 && canh2 == canh3) {
    contentHTML = `<p> Tam giác đều</p>`;
  } else if (canh1 == canh2 || canh1 == canh3 || canh2 == canh3) {
    if (
      Math.floor(
        Math.pow(canh1, 2) +
          Math.pow(canh2, 2) +
          Math.pow(canh3, 2) -
          2 * Math.pow(Math.max(canh1, canh2, canh3), 2)
      ) == 0
    ) {
      contentHTML = `<p> Tam giác vuông cân</p>`;
    } else {
      contentHTML = `<p> Tam giác cân</p>`;
    }
  } else if (
    Math.pow(canh1, 2) + Math.pow(canh2, 2) + Math.pow(canh3, 2) ==
    2 * Math.pow(Math.max(canh1, canh2, canh3), 2)
  ) {
    contentHTML = `<p> Tam giác vuông</p>`;
  } else {
    contentHTML = `<p> Tam giác thường</p>`;
  }
  document.getElementById("ketQuaBai4").innerHTML = contentHTML;
}
/**Bài 5*/
var contentHTML;
var arrDate = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
function inKetQuaBai5(dayCal) {
  var day = document.getElementById("day").value * 1;
  var month = document.getElementById("month").value * 1;
  var year = document.getElementById("year").value * 1;
  var dateCheck = new Date(year, month - 1, day);
  dayCal(dateCheck);
}
var ngayTiepTheo = function (dateCheck) {
  dateCheck.setDate(dateCheck.getDate() + 1);
  contentHTML = `<p> ${dateCheck.getDate()}/${
    dateCheck.getMonth() + 1
  }/${dateCheck.getFullYear()}</p>`;
  document.getElementById("ketQuaBai56").innerHTML = contentHTML;
};
var ngayTruocDo = function (dateCheck) {
  dateCheck.setDate(dateCheck.getDate() - 1);
  contentHTML = `<p> ${dateCheck.getDate()}/${
    dateCheck.getMonth() + 1
  }/${dateCheck.getFullYear()}</p>`;
  document.getElementById("ketQuaBai56").innerHTML = contentHTML;
};
function inKetQuaBai6(dayCal) {
  var month = document.getElementById("month").value * 1;
  var year = document.getElementById("year").value * 1;
  if (year % 4 == 0) {
    arrDate[1] = 29;
  }
  contentHTML = `<p> Tháng ${month} có ${arrDate[month - 1]} ngày</p>`;
  document.getElementById("ketQuaBai56").innerHTML = contentHTML;
}
/**Bài 7*/
var contentHTML;
var arr = ["Một", "Hai", "Ba", "Bốn", "Năm", "Sáu", "Bảy", "Tám", "Chín"];
function inKetQuaBai7() {
  var threeDigitNumber = document.getElementById("threeDigitNumber").value * 1;
  var hundred = Math.floor(threeDigitNumber / 100);
  var ten = Math.floor((threeDigitNumber % 100) / 10);
  var unit = threeDigitNumber % 10;
  if (ten == 1) {
    if (unit == 0) {
      contentHTML = `<p> ${arr[hundred - 1]} trăm mười</p>`;
    } else {
      contentHTML = `<p> ${arr[hundred - 1]}  trăm mười ${arr[
        unit - 1
      ].toLowerCase()}</p>`;
    }
  } else if (unit == 1) {
    contentHTML = `<p> ${arr[hundred - 1]} trăm ${arr[
      ten - 1
    ].toLowerCase()} mươi mốt</p>`;
  } else if (unit == 5) {
    contentHTML = `<p> ${arr[hundred - 1]} trăm ${arr[
      ten - 1
    ].toLowerCase()} mươi lăm</p>`;
  } else if (unit == 0) {
    contentHTML = `<p> ${arr[hundred - 1]} trăm ${arr[
      ten - 1
    ].toLowerCase()} mươi
      </p>`;
  } else {
    contentHTML = `<p> ${arr[hundred - 1]} trăm ${arr[
      ten - 1
    ].toLowerCase()} mươi ${arr[unit - 1].toLowerCase()}
    </p>`;
  }
  document.getElementById("ketQuaBai7").innerHTML = contentHTML;
}
/**Bài 8*/
var contentHTML;
function inKetQuaBai8() {
  var toaDoXTruong = document.getElementById("trX").value * 1;
  var toaDoYTruong = document.getElementById("trY").value * 1;
  var distance = 0;
  for (var i = 1; i <= 3; i++) {
    var x = document.getElementById("x" + i).value * 1;
    var y = document.getElementById("y" + i).value * 1;
    var studentName = document.getElementById("name" + i).value;
    var d = Math.sqrt(
      Math.pow(x - toaDoXTruong, 2) + Math.pow(y - toaDoYTruong, 2)
    );
    if (d > distance) {
      distance = d;
      contentHTML = `Sinh viên xa trường nhất là: ${studentName}`;
    }
  }
  document.getElementById("ketQuaBai8").innerHTML = `<p> ${contentHTML}</p>`;
}
