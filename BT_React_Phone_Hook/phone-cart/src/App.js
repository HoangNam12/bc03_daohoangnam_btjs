import logo from "./logo.svg";
import "./App.css";
import PhoneCart from "./phone_cart/PhoneCart";

function App() {
  return (
    <div className="App">
      <PhoneCart />
    </div>
  );
}

export default App;
