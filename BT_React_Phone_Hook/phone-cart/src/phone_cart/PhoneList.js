import React from "react";
import PhoneItem from "./PhoneItem";

export default function PhoneList({
  productData,
  handleAddToCart,
  showDetail,
}) {
  return (
    <div className="container">
      <h1 className="text-primary my-4 text-3xl">Danh sách sản phẩm</h1>
      <div className="row">
        {productData.map((item, index) => {
          return (
            <PhoneItem
              data={item}
              key={index}
              handleAddToCart={handleAddToCart}
              showDetail={showDetail}
            />
          );
        })}
      </div>
    </div>
  );
}
