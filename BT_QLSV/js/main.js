var danhSachSinhVien = [];
const DSSV_LOCALSTORAGE = "DSSV_LOCALSTORAGE";
var validatorSv = new ValidationSV();

//Lay du lieu tu LOCALSTORAGE khi user tai lai trang

const timKiemViTri = function (id, array) {
  return array.findIndex(function (sv) {
    return sv.maSv == id;
  });
};

const luuLocalStorage = function () {
  //convert array thanh Json de co the luu vao localStorage
  var dssvJson = JSON.stringify(danhSachSinhVien);
  localStorage.setItem(DSSV_LOCALSTORAGE, dssvJson);
};

var dssvJson = localStorage.getItem(DSSV_LOCALSTORAGE);
if (dssvJson) {
  danhSachSinhVien = JSON.parse(dssvJson);
  danhSachSinhVien = danhSachSinhVien.map(function (item) {
    return new SinhVien(
      item.maSv,
      item.tenSv,
      item.emailSv,
      item.diemToan,
      item.diemLy,
      item.diemHoa
    );
  });
  xuatDanhSachSinhVien(danhSachSinhVien);
}

function themSinhVien() {
  var newSinhVien = layThongTinTuForm();
  var isValidMaSV =
    validatorSv.kiemtraRong(
      "txtMaSV",
      "spanMaSV",
      "Mã sinh viên không được rỗng"
    ) && validatorSv.kiemTraIdHopLe(newSinhVien, danhSachSinhVien);
  var isValid = isValidMaSV && checkValid();
  if (isValid) {
    danhSachSinhVien.push(newSinhVien);
    xuatDanhSachSinhVien(danhSachSinhVien);
    luuLocalStorage();
  }
}

function checkValid() {
  var isValidEmailSV =
    validatorSv.kiemtraRong(
      "txtEmail",
      "spanEmailSV",
      "Email không được rỗng"
    ) && validatorSv.kiemTraEmail("txtEmail", "spanEmailSV");
  var isValidTenSV = validatorSv.kiemtraRong(
    "txtTenSV",
    "spanTenSV",
    "Tên sinh viên không được rỗng"
  );
  var isValidDiemToan = validatorSv.kiemtraRong(
    "txtDiemToan",
    "spanToan",
    "Điểm toán không được rỗng"
  );
  var isValidDiemLy = validatorSv.kiemtraRong(
    "txtDiemLy",
    "spanLy",
    "Điểm lý không được rỗng"
  );
  var isValidDiemHoa = validatorSv.kiemtraRong(
    "txtDiemHoa",
    "spanHoa",
    "Điểm hoá không được rỗng"
  );
  return (
    isValidEmailSV &&
    isValidTenSV &&
    isValidDiemToan &&
    isValidDiemLy &&
    isValidDiemHoa
  );
}

function xoaSinhVien(id) {
  var viTri = timKiemViTri(id, danhSachSinhVien);
  danhSachSinhVien.splice(viTri, 1);
  xuatDanhSachSinhVien(danhSachSinhVien);
  luuLocalStorage();
}

function suaSinhVien(id) {
  var viTri = timKiemViTri(id, danhSachSinhVien);
  var sinhVien = danhSachSinhVien[viTri];
  xuatThongTinForm(sinhVien);
}

function capNhatSinhVien() {
  var sinhVienEdit = layThongTinTuForm();
  var viTri = timKiemViTri(sinhVienEdit.maSv, danhSachSinhVien);
  danhSachSinhVien[viTri] = sinhVienEdit;
  var isValid = checkValid();
  if (isValid) {
    xuatDanhSachSinhVien(danhSachSinhVien);
    luuLocalStorage();
    resetSinhVien();
  }
}

function resetSinhVien() {
  document.getElementById("txtMaSV").value = "";
  document.getElementById("txtTenSV").value = "";
  document.getElementById("txtEmail").value = "";
  document.getElementById("txtDiemToan").value = "";
  document.getElementById("txtDiemLy").value = "";
  document.getElementById("txtDiemHoa").value = "";
  document.getElementById("spanMaSV").innerHTML = "";
  document.getElementById("spanTenSV").innerHTML = "";
  document.getElementById("spanEmailSV").innerHTML = "";
  document.getElementById("spanToan").innerHTML = "";
  document.getElementById("spanLy").innerHTML = "";
  document.getElementById("spanHoa").innerHTML = "";
}
function timSinhVien(tenSv, array) {
  var list = array.filter(function (sv) {
    return sv.tenSv.toLowerCase() == tenSv.toLowerCase();
  });
  return list;
}
document.getElementById("btnSearch").addEventListener(
  "click",
  function () {
    var tenSV = document.getElementById("txtSearch").value;
    var searchList = [];
    if (tenSV) {
      searchList = timSinhVien(tenSV, danhSachSinhVien);
      console.log(searchList);
      if (searchList != "") {
        xuatDanhSachSinhVien(searchList);
        document.getElementById("spansearch").textContent = "";
        document.getElementById("txtSearch").value = tenSV;
        return true;
      }
      document.getElementById("spansearch").textContent =
        "Sinh viên không tồn tại";
      return false;
    }
    document.getElementById("spansearch").textContent =
      "Nhập tên sinh viên cần tìm";
  },
  false
);
document.getElementById("btnCancel").addEventListener(
  "click", function() {
     document.getElementById("txtSearch").value = "";
      xuatDanhSachSinhVien(danhSachSinhVien);
  });